﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactAPIDAL.Interfaces
{
    public interface IRepository <TKey, TEntity> :IDisposable
        where TEntity: IEntity<TKey>
    {
        TEntity Get(TKey id);
        IEnumerable<TEntity> GetAll();
        TKey Insert(TEntity item);
        bool Update(TEntity item);
        bool Delete(TKey id);
    }
}
