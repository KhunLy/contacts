﻿using ContactAPIDAL.Entities;
using DB;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactAPIDAL.Repositories
{
    public class ContactRepository : BaseRepository<int, Contact>
    {

        public ContactRepository(Connection connection) : base(connection)
        {
        }

        public override bool Delete(int id)
        {
            string query = "DELETE FROM Contact WHERE Id = @id";
            Command cmd = new Command(query, false);
            cmd.AddParameter("id", id);
            return _Connection.ExecuteNonQuery(cmd) == 1;
        }

        public override Contact Get(int id)
        {
            string query = "SELECT * FROM Contact WHERE Id = @id";
            Command cmd = new Command(query, false);
            cmd.AddParameter("id", id);
            return _Connection.ExecuteReader(cmd, Selector).FirstOrDefault();
        }

        public override IEnumerable<Contact> GetAll()
        {
            string query = "SELECT * FROM Contact";
            Command cmd = new Command(query, false);
            return _Connection.ExecuteReader(cmd, Selector);
        }

        public override int Insert(Contact item)
        {
            string query = "INSERT INTO Contact(Name,FirstName,Phone) OUTPUT inserted.Id VALUES (@name, @firstName, @phone)";
            Command cmd = new Command(query, false);
            cmd.AddParameter("name", item.Name);
            cmd.AddParameter("firstName", item.FirstName);
            cmd.AddParameter("phone", item.Phone);
            return (int)_Connection.ExecuteScalar(cmd);
        }

        public override bool Update(Contact item)
        {
            string query = "UPDATE Contact SET Name = @name, FirstName= @firstName, Phone = @phone, IsBusy = @isBusy WHERE Id = @id";
            Command cmd = new Command(query, false);
            cmd.AddParameter("name", item.Name);
            cmd.AddParameter("firstName", item.FirstName);
            cmd.AddParameter("phone", item.Phone);
            cmd.AddParameter("isBusy", item.IsBusy);
            cmd.AddParameter("id", item.Id);
            return _Connection.ExecuteNonQuery(cmd) == 1;
        }

        private Contact Selector(IDataReader arg)
        {
            return new Contact
            {
                Id = (int)arg["Id"],
                FirstName = arg["FirstName"] as string,
                Name = arg["Name"] as string,
                Phone = arg["Phone"] as string,
                IsBusy = (bool)arg["IsBusy"],
            };
        }
    }
}
