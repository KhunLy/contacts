﻿using ContactAPIDAL.Interfaces;
using DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactAPIDAL.Repositories
{
    public abstract class BaseRepository<TKey, TEntity>
        : IRepository<TKey, TEntity>, IDisposable
        where TEntity : IEntity<TKey>
    {
        protected readonly Connection _Connection;

        public BaseRepository(Connection connection)
        {
            _Connection = connection;
        }

        public abstract TEntity Get(TKey id);

        public abstract IEnumerable<TEntity> GetAll();

        public abstract TKey Insert(TEntity item);

        public abstract bool Update(TEntity item);

        public abstract bool Delete(TKey id);

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
