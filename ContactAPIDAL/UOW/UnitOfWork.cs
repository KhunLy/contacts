﻿using ContactAPIDAL.Entities;
using ContactAPIDAL.Interfaces;
using ContactAPIDAL.Repositories;
using DB;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace ContactAPIDAL.UOW
{
    public class UnitOfWork : IDisposable
    {
        public IRepository<int, Contact> ContactRepository { get; private set; }

        private TransactionScope _Transaction;

        public UnitOfWork()
        {
            Connection connection = new Connection(
                ConfigurationManager.ConnectionStrings["default"].ConnectionString,
                ConfigurationManager.ConnectionStrings["default"].ProviderName
            );
            ContactRepository = new ContactRepository(connection);
            _Transaction = new TransactionScope();
        }

        public bool SaveChanges()
        {
            try
            {
                _Transaction.Complete();
            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                _Transaction.Dispose();
                _Transaction = new TransactionScope();
            }
            return true;
        }

        private bool _Disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (_Disposed)
                return;
            if (disposing)
            {
                ContactRepository.Dispose();
                _Transaction.Dispose();
            }
            _Disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
