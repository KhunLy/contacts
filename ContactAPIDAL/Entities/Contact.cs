﻿using ContactAPIDAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactAPIDAL.Entities
{
    public class Contact: IEntity<int>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string FirstName { get; set; }
        public string Phone { get; set; }
        public bool IsBusy { get; set; }
    }
}
