﻿using ContactsDAL.Providers;
using ContactsInterfaces.Navigation;
using ContactsInterfaces.Providers;
using ContactsInterfaces.ViewModels;
using ContactsViewModels.ViewModels;
using ContactsWPF.Navigation;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactsWPF.DI
{
    public class ServicesLocator
    {
        private static readonly IServiceCollection
            _Collection;
        private static IServiceProvider _Container;
        public static IServiceProvider Container
        {
            get
            {
                return _Container = _Container ??
                    _Collection.BuildServiceProvider();
            }
        }

        static ServicesLocator()
        {
            _Collection = new ServiceCollection();
            RegisterServices();
        }

        private static void RegisterServices()
        {
            _Collection.AddSingleton<INavigationService, NavigationService>();
            _Collection.AddSingleton<IContactProvider, ContactProvider>();
            _Collection.AddTransient<IContactsViewModel, ContactsViewModel>();
            _Collection.AddTransient<IAddContactViewModel, AddContactViewModel>();
            _Collection.AddTransient<IDetailsContactViewModel, DetailsContactViewModel>();
        }
    }
}
