﻿using ContactsInterfaces.Navigation;
using ContactsWPF.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactsWPF.Navigation
{
    public class NavigationService : INavigationService
    {
        private AddContactWindow _CurrentAddWindow;

        public void CloseAddWindow()
        {
            _CurrentAddWindow.Close();
        }

        public void OpenCreateContact()
        {
            _CurrentAddWindow = new AddContactWindow();
            _CurrentAddWindow.Show();
        }

        public void OpenDetailsContact(int id)
        {
            DetailsContactWindow w = new DetailsContactWindow();
            w.Show();
        }
    }
}
