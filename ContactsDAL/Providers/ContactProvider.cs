﻿using ContactsDAL.Entities;
using ContactsInterfaces.Models;
using ContactsInterfaces.Providers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ContactsDAL.Providers
{
    public class ContactProvider : IContactProvider
    {
        private string _EndPoint = "http://localhost:10334/api/contact/";

        public async Task Delete(int id)
        {
            using (HttpClient client = new HttpClient())
            {
                await client.DeleteAsync(_EndPoint + id);
            }
        }

        public async Task<IContact> Get(int id)
        {
            using (HttpClient client = new HttpClient())
            {
                HttpResponseMessage resp = await client.GetAsync(_EndPoint + id);
                if (resp.IsSuccessStatusCode)
                {
                    string json = await resp.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<Contact>(json);
                }
                return null;
            }
        }

        public async Task<IEnumerable<IContact>> GetAll()
        {
            using (HttpClient client = new HttpClient())
            {
                HttpResponseMessage resp = await client.GetAsync(_EndPoint);
                if (resp.IsSuccessStatusCode)
                {
                    string json = await resp.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<IEnumerable<Contact>>(json);
                }
                return new List<Contact>();
            }
        }

        public async Task<int> Post(IContact item)
        {
            using (HttpClient client = new HttpClient())
            {
                string myString = JsonConvert.SerializeObject(item);
                StringContent content 
                    = new StringContent(myString, Encoding.UTF8, "application/json");
                HttpResponseMessage resp = await client.PostAsync(_EndPoint, content);
                if (resp.IsSuccessStatusCode)
                {
                    string json = await resp.Content.ReadAsStringAsync();
                    return int.Parse(json); 
                }
                return 0;
            }
        }
    }
}
