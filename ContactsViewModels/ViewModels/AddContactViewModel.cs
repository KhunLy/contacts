﻿using ContactsInterfaces.Models;
using ContactsInterfaces.Navigation;
using ContactsInterfaces.Providers;
using ContactsInterfaces.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using ToolBox.MVVM.Bindable;
using ToolBox.MVVM.Commands;
using ToolBox.MVVM.Mediators;

namespace ContactsViewModels.ViewModels
{
    public class AddContactViewModel : BindableBase, IAddContactViewModel, IContact
    {
        public string Name { get; set; }
        public string FirstName { get; set; }
        public string Phone { get; set; }
        public bool IsBusy { get; set; }

        [JsonIgnore]
        public ICommand SaveCmd { get; set; }

        public int Id { get; set; }

        private IContactProvider _Provider;

        private INavigationService _NavService;


        public AddContactViewModel(IContactProvider provider, INavigationService navService)
        {
            _Provider = provider;
            _NavService = navService;
            SaveCmd = new RelayCommand(Save);
        }

        private async void Save()
        {
            Id = await _Provider.Post(this);
            if(Id != 0)
            {
                MessagingCenter<IContact>.Default.Send("NEW_CONTACT", this);
                // fermer fenetre 
                _NavService.CloseAddWindow();
            }
            else
            {
                // affiche un message d'erreur
            }
        }
    }
}
