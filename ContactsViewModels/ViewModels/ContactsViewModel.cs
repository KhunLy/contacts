﻿using ContactsInterfaces.Models;
using ContactsInterfaces.Navigation;
using ContactsInterfaces.Providers;
using ContactsInterfaces.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using ToolBox.MVVM.Bindable;
using ToolBox.MVVM.Commands;
using ToolBox.MVVM.Mediators;

namespace ContactsViewModels.ViewModels
{
    public class ContactsViewModel : BindableBase, IContactsViewModel
    {
        private ObservableCollection<IContact> _Contacts;
        public ObservableCollection<IContact> Contacts {
            get { return _Contacts; }
            set { SetValue(ref _Contacts, value); }
        }

        public ICommand OpenCreateContactCmd { get; set; }

        public ICommand DeleteContactCmd { get; set; }

        public ICommand OpenContactDetailsCmd { get; set; }

        private IContactProvider _Provider;
        private INavigationService _Nav;

        private bool _CanOpenAddWindow;

        public bool CanOpenAddWindow
        {
            get { return _CanOpenAddWindow; }
            set {
                _CanOpenAddWindow = value;

                ((RelayCommand)OpenCreateContactCmd).RaiseCanExecuteChanged(this); 
            }
        }


        public ContactsViewModel(
            IContactProvider provider,
            INavigationService navService
        )
        {
            _Provider = provider;
            _Nav = navService;
            MessagingCenter<IContact>.Default.Subscribe("NEW_CONTACT", AddContact);
            MessagingCenter<bool>.Default.Subscribe("ADD_WINDOW_CLOSE", (val) => CanOpenAddWindow = true);
            OpenCreateContactCmd 
                = new RelayCommand(
                    OpenCreateWindow, 
                    () => CanOpenAddWindow
                );
            CanOpenAddWindow = true;

            DeleteContactCmd = new RelayCommand<int>(DeleteContact);
            Contacts = new ObservableCollection<IContact>();
            LoadItems();
        }

        private async void DeleteContact(int id)
        {
            await _Provider.Delete(id);
            Contacts.Remove(Contacts.FirstOrDefault(c => c.Id == id));
        }

        private void AddContact(IContact obj)
        {
            Contacts.Add(obj);
        }

        private void OpenCreateWindow()
        {
            //ouvrir la fenetre AddContactWindow
            _Nav.OpenCreateContact();
            CanOpenAddWindow = false;
        }

        private async void LoadItems()
        {
            Contacts = new ObservableCollection<IContact>(
                await _Provider.GetAll()
            );
        }
    }
}
