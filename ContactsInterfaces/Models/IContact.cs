﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactsInterfaces.Models
{
    public interface IContact
    {
        int Id { get; set; }
        string Name { get; set; }
        string FirstName { get; set; }
        string Phone { get; set; }
        bool IsBusy { get; set; }
    }
}
