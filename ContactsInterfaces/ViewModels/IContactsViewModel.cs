﻿using ContactsInterfaces.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ContactsInterfaces.ViewModels
{
    public interface IContactsViewModel
    {
        ObservableCollection<IContact> Contacts { get; }

        ICommand OpenCreateContactCmd { get; }

        ICommand DeleteContactCmd { get; }

        ICommand OpenContactDetailsCmd { get; }
    }
}
