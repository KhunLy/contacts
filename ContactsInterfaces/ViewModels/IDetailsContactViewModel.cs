﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactsInterfaces.ViewModels
{
    public interface IDetailsContactViewModel
    {
        string Name { get; }

        string FirstName { get; }

        string Phone { get; }

        bool IsBusy { get; }
    }
}
