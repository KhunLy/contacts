﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ContactsInterfaces.ViewModels
{
    public interface IAddContactViewModel
    {
        string Name { get; set; }

        string FirstName { get; set; }

        string Phone { get; set; }

        bool IsBusy { get; set; }

        ICommand SaveCmd { get; }
    }
}
