﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactsInterfaces.Navigation
{
    public interface INavigationService
    {
        void OpenDetailsContact(int id);
        void OpenCreateContact();
        void CloseAddWindow();
    }
}
