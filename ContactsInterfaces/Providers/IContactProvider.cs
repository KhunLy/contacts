﻿using ContactsInterfaces.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactsInterfaces.Providers
{
    public interface IContactProvider
    {
        Task<IContact> Get(int id);
        Task<IEnumerable<IContact>> GetAll();
        Task<int> Post(IContact item);
        Task Delete(int id);
    }
}
