﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB
{
    public class Command
    {
        #region Fields
        private string _query;
        #endregion
        #region Properties
        public string Query {
            get => _query;
            set {
                if (string.IsNullOrWhiteSpace(value))
                    throw new Exception("Requête invalide");
                _query = value; }
        }
        public Dictionary<string, object> Parameters { get; set; }
        public bool IsStoredProcedure { get; set; }
        #endregion
        #region Constructors
        public Command(string query, bool isStoredProcedure = true)
        {
            this.Query = query;
            this.IsStoredProcedure = isStoredProcedure;
            this.Parameters = new Dictionary<string, object>();
        }

        public Command(string query, Dictionary<string,object> parameters, bool isStoredProcedure=true):this(query,isStoredProcedure)
        {
            if(parameters !=null)
            {
                foreach (KeyValuePair<string,object> kvp in parameters)
                {
                    this.AddParameter(kvp.Key, kvp.Value);
                }
            }
            
        }
        #endregion
        #region Methods
        /// <summary>
        /// Ajoute un paramètre à la propriété Parameters de la class Commande si celui-ci est valide.
        /// </summary>
        /// <param name="name">Nom du paramètre</param>
        /// <param name="value">Valeur du paramètre</param>
        public void AddParameter(string name, object value)
        {
            if (!string.IsNullOrWhiteSpace(name))
            {
                this.Parameters.Add(name, value??DBNull.Value);
            }
        }
        #endregion
    }
}
