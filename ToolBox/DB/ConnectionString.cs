﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB
{
    public class ConnectionString
    {
        public string DataSource { get; set; }
        public string InitialCatalog { get; set; }
        public string IntegratedSecurity { get; set; }
        public bool MultipleActiveResultSet { get; set; }
        public string UserId { get; set; }
        public string Password { get; set; }
    }
}
