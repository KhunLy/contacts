﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB
{
    public class Connection
    {
        #region Fields
        private string _connectionString;
        private DbProviderFactory Factory;
        #endregion
        #region Properties
        private string ConnectionString {
            get => _connectionString;
            set {
                DbConnection connection = Factory.CreateConnection();
                connection.ConnectionString = value;
                connection.Open();
                if (connection.State != ConnectionState.Open)
                    throw new Exception("Connection String invalide");
                _connectionString = value;
                connection.Dispose();
            } }
        #endregion
        #region Constructors
        public Connection(string connectionString, string invariantName="System.Data.SqlClient")
        {
            if (string.IsNullOrWhiteSpace(invariantName))
                throw new Exception("Invariant Name invalide");
            this.Factory = DbProviderFactories.GetFactory(invariantName);
            this.ConnectionString = connectionString;
        }
        #endregion
        #region Methods
        public object ExecuteScalar (Command cmd)
        {
            using (DbConnection connection = this.CreateConnection())
            {
                using (DbCommand command = this.CreateCommand(connection,cmd))
                {
                    return command.ExecuteScalar();
                }
            }
        }

        public int ExecuteNonQuery (Command cmd)
        {
            using(DbConnection connection = this.CreateConnection())
            {
                using (DbCommand command = this.CreateCommand(connection,cmd))
                {
                    return command.ExecuteNonQuery();
                }
            }
        }

        public DataTable GetDataTable (Command cmd)
        {            
            using(DbConnection connection = this.CreateConnection())
            {
                using (DbCommand command = this.CreateCommand(connection,cmd))
                {
                    DataTable result = new DataTable();
                    DbDataAdapter adapter = Factory.CreateDataAdapter();
                    adapter.SelectCommand = command;
                    adapter.Fill(result);
                    return result;
                }
            }
        }

        public DataSet GetDataSet(Command cmd)
        {
            using (DbConnection connection = this.CreateConnection())
            {
                using (DbCommand command = this.CreateCommand(connection, cmd))
                {
                    DataSet result = new DataSet();
                    DbDataAdapter adapter = Factory.CreateDataAdapter();
                    adapter.SelectCommand = command;
                    adapter.Fill(result);
                    return result;
                }
            }
        }

        public IEnumerable<T> ExecuteReader<T>(Command cmd, Func<IDataReader,T> convert) where T : new()
        {
            using (DbConnection connection = this.CreateConnection())
            {
                using (DbCommand command = this.CreateCommand(connection,cmd))
                {
                    using (DbDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            yield return convert(reader);
                        }
                    }
                }
            }
        }

        private DbConnection CreateConnection()
        {
            DbConnection connection = Factory.CreateConnection();
            connection.ConnectionString = this.ConnectionString;
            connection.Open();
            return connection;
        }

        private DbCommand CreateCommand(DbConnection connection, Command cmd)
        {
            DbCommand command = connection.CreateCommand();
            command.CommandText = cmd.Query;
            command.CommandType = (cmd.IsStoredProcedure) ? CommandType.StoredProcedure : CommandType.Text;
            foreach (KeyValuePair<string, object> kvp in cmd.Parameters)
            {
                DbParameter param = Factory.CreateParameter();
                param.ParameterName = kvp.Key;
                param.Value = kvp.Value;
                command.Parameters.Add(param);
            }
            return command;
        }
        #endregion
    }
}
