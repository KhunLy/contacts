﻿using DB;
using DebugDB.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DebugDB
{
    class Program
    {
        const string CONNECTIONSTRING = @"Data Source=ZBOOK-4944\SQLEXPRESS;Initial Catalog=ADO;Persist Security Info=True;User ID=sa;Password=test1234=";

        static void Main(string[] args)
        {
            Connection connection = new Connection(CONNECTIONSTRING);
            Command command = new Command("SELECT Id, FirstName, Lastname FROM Student", false);
            IEnumerable<Student> students = connection.ExecuteReader<Student>(command, (IDataReader student) => new Student()
            {
                Id = (int)student["Id"],
                FirstName = (string)student["FirstName"],
                LastName = (string)student["LastName"]
            });
            foreach (Student student in students)
            {
                Console.WriteLine($"{student.Id} | {student.FirstName}, {student.LastName}");
            }

            Console.ReadLine();
        }
    }
}
