﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToolBox.MVVM.Mediators
{
    public class MessagingCenter<T>
    {
        #region Default pattern
        private static MessagingCenter<T> _Instance;

        private static readonly object _Lock = new object();

        public static MessagingCenter<T> Default
        {
            get {
                lock (_Lock)
                {
                    return _Instance = _Instance
                    ?? new MessagingCenter<T>();
                }
            }
        }

        public MessagingCenter()
        {
            _Events = new Dictionary<string, Action<T>>();
        }
        #endregion

        private readonly Dictionary<string, Action<T>> _Events;

        public void Subscribe(string topic, Action<T> action)
        {
            if (_Events.ContainsKey(topic))
                _Events[topic] += action;
            else
                _Events.Add(topic, action);
        }

        public void Send(string topic, T item)
        {
            if (_Events.ContainsKey(topic))
                _Events[topic]?.Invoke(item);
        }

        public void Unsubscribe(string topic, Action<T> action)
        {
            if (_Events.ContainsKey(topic))
                _Events[topic] -= action;
        }
    }
}
