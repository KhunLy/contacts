﻿using ContactAPIDAL.Entities;
using ContactAPIDAL.UOW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ContactsAPI.Controllers
{
    public class ContactController : ApiController
    {
        private readonly UnitOfWork _UOW;
        public ContactController()
        {
            _UOW = new UnitOfWork();
        }

        protected override void Dispose(bool disposing)
        {
            if(disposing)
                _UOW.Dispose();
            base.Dispose(disposing);
        }

        public IHttpActionResult Get(int id)
        {
            Contact c = _UOW.ContactRepository.Get(id);
            if (c == null)
                return NotFound();
            return Ok(c);
        }

        public IHttpActionResult Get()
        {
            IEnumerable<Contact> res = _UOW.ContactRepository.GetAll();
            return Ok(res);
        }

        public IHttpActionResult Post([FromBody] Contact c)
        {
            if (ModelState.IsValid)
            {
                int id = _UOW.ContactRepository.Insert(c);
                _UOW.SaveChanges();
                return Ok(id);
            }
            return BadRequest(ModelState);
        }

        public IHttpActionResult Put([FromBody] Contact c)
        {
            if (ModelState.IsValid)
            {
                _UOW.ContactRepository.Update(c);
                _UOW.SaveChanges();
                return Ok(c.Id);
            }
            return BadRequest(ModelState);
        }

        public IHttpActionResult Delete(int id)
        {
            if (_UOW.ContactRepository.Delete(id))
            {
                _UOW.SaveChanges();
                return Ok(id);
            }
            return NotFound();
        }
    }
}
